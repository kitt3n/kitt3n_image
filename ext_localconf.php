<?php
defined('TYPO3_MODE') || die('Access denied.');

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

call_user_func(
    function ($extKey, $globals) {

        // wizards
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
                wizards.newContentElement.wizardItems {
                    KITT3N {
                        header = KITT3N
                        after = common,special,menu,plugins,forms
                        elements {
                            kitt3n_image {
                                iconIdentifier = kitt3n_svg_big
                                title = kitt3n | Image
                                description = Image using <picture> tag. The extension also provides two different ViewHelpers.
                                tt_content_defValues {
                                    CType = kitt3n_image
                                }
                            }
                        }
                        show := addToList(kitt3n_image)
                    }

                }
            }'
        );

//        // Register for hook to show preview of tt_content element of CType="yourextensionkey_newcontentelement" in page module
//        $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['tt_content_drawItem']['hnyfoo_foo'] =
//            \HNY\HnyFoo\Hooks\PageLayoutView\NewContentElementPreviewRenderer::class;

    }, $_EXTKEY, $GLOBALS
);