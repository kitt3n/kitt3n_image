<?php
namespace KITT3N\Kitt3nImage\Service;

/**
 * Class PictureService
 * @package KITT3N\Kitt3nImage\Service
 */
class LazyImageService implements \TYPO3\CMS\Core\SingletonInterface
{
    /**
     * processService
     *
     * @var \KITT3N\Kitt3nImage\Service\ProcessService
     * @inject
     */
    protected $processService = null;

    public function returnLazyImageHtml ($aArguments)
    {

        $sHtml = '';

        $aProcessed = $this->processService->process($aArguments);

        if ( ! null == $aProcessed) {

            $aProcessedImageClasses = [];
            foreach ($aProcessed[1] as $iProcessedImage => $aProcessedImage) {
                $aProcessedImageClasses[] = $aProcessedImage["classes"];
            }

            $aProcessedImageCsss = [];

            /*
             * Fallback for
             * - IEs 9 and lower
             * - Edge 12-15
             */
            $aProcessedImageFallbackCsss = [];

            /*
             * Fallback for
             * - IEs 10 and 11
             */
            $aProcessedImageIE10PlusFallbackCsss = [];

            $aProcessedImageFallbackCsss[] =
                '#garfield-' . $aArguments['fileReferenceUid'] . '.tx-kitt3n-image > .aR > img { display: none;}';

            $aProcessedImageIE10PlusFallbackCsss[] =
                '@media all and (-ms-high-contrast: none), (-ms-high-contrast: active) {' .
                '#garfield-' . $aArguments['fileReferenceUid'] . '.tx-kitt3n-image > .aR > img { display: none;}}';

            /*
             * small
             */
            if ($aProcessed[1][2]["pictureStyle"] != '') {
                $aProcessedImageCsss[] =
                    '@media (max-width: 991px) { #garfield-' . $aArguments['fileReferenceUid'] . '.tx-kitt3n-image > .aR.aR--0-x_y {' . $aProcessed[1][2]["pictureStyle"] . '}} ';
            }
            $aProcessedImageCsss[] =
                '#garfield-' . $aArguments['fileReferenceUid'] . '.tx-kitt3n-image > .aR > img {' . $aProcessed[1][2]["style"] . '}';

            $aProcessedImageFallbackCsss[] =
                '#garfield-' . $aArguments['fileReferenceUid'] . '.tx-kitt3n-image > .aR { background: url(' .
                (array_key_exists("frontendPathSymlink", $aProcessed[1][2]) ?
                    $aProcessed[1][2]["frontendPathSymlink"] : $aProcessed[1][2]["frontendPath"]) .
                ') no-repeat; background-size: cover; ' .
                (str_replace("object-position", "background-position",  $aProcessed[1][2]["style"])) . '}';

            $aProcessedImageIE10PlusFallbackCsss[] =
                '@media all and (-ms-high-contrast: none), (-ms-high-contrast: active) {' .
                '#garfield-' . $aArguments['fileReferenceUid'] . '.tx-kitt3n-image > .aR { background: url(' .
                (array_key_exists("frontendPathSymlink", $aProcessed[1][2]) ?
                    $aProcessed[1][2]["frontendPathSymlink"] : $aProcessed[1][2]["frontendPath"]) .
                ') no-repeat; background-size: cover; ' . (str_replace("object-position", "background-position",  $aProcessed[1][2]["style"])) . '}}';

            /*
             * large
             */
            if ($aProcessed[1][1]["pictureStyle"] != '') {
                $aProcessedImageCsss[] =
                    '@media (min-width: 992px) and (max-width: 1199px) { #garfield-' . $aArguments['fileReferenceUid'] . '.tx-kitt3n-image > .aR.aR--0-x_y {' . $aProcessed[1][1]["pictureStyle"] . '}}';
            }
            $aProcessedImageCsss[] =
                '@media (min-width: 992px) { #garfield-' . $aArguments['fileReferenceUid'] . '.tx-kitt3n-image > .aR > img {' . $aProcessed[1][1]["style"] . '}}';

            $aProcessedImageFallbackCsss[] =
                '@media (min-width: 992px) and (max-width: 1199px) { #garfield-' . $aArguments['fileReferenceUid'] . '.tx-kitt3n-image > .aR { background: url(' . (array_key_exists("frontendPathSymlink", $aProcessed[1][1]) ?
                    $aProcessed[1][1]["frontendPathSymlink"] : $aProcessed[1][1]["frontendPath"]) . ') no-repeat; background-size: cover; ' . (str_replace("object-position", "background-position",  $aProcessed[1][1]["style"])) . '}}';

            $aProcessedImageIE10PlusFallbackCsss[] =
                '@media (min-width: 992px) and (max-width: 1199px) and (-ms-high-contrast: none), (-ms-high-contrast: active) { #garfield-' . $aArguments['fileReferenceUid'] . '.tx-kitt3n-image > .aR { background: url(' .
                (array_key_exists("frontendPathSymlink", $aProcessed[1][1]) ?
                    $aProcessed[1][1]["frontendPathSymlink"] : $aProcessed[1][1]["frontendPath"]) .
                ') no-repeat; background-size: cover; ' . (str_replace("object-position", "background-position",  $aProcessed[1][1]["style"])) . '}}';

            /*
             * extra large
             */
            if ($aProcessed[1][0]["pictureStyle"] != '') {
                $aProcessedImageCsss[] =
                    '@media (min-width: 1200px) { #garfield-' . $aArguments['fileReferenceUid'] . '.tx-kitt3n-image > .aR.aR--0-x_y {' . $aProcessed[1][0]["pictureStyle"] . '}}';
            }
            $aProcessedImageCsss[] =
                '@media (min-width: 1200px) { #garfield-' . $aArguments['fileReferenceUid'] . '.tx-kitt3n-image > .aR > img {' . $aProcessed[1][0]["style"] . '}}';

            $aProcessedImageFallbackCsss[] =
                '@media (min-width: 1200px) { #garfield-' . $aArguments['fileReferenceUid'] . '.tx-kitt3n-image > .aR { background: url(' . (array_key_exists("frontendPathSymlink", $aProcessed[1][0]) ?
                    $aProcessed[1][0]["frontendPathSymlink"] : $aProcessed[1][0]["frontendPath"]) . ') no-repeat; background-size: cover; ' . (str_replace("object-position", "background-position",  $aProcessed[1][0]["style"])) . '}}';

            $aProcessedImageIE10PlusFallbackCsss[] =
                '@media (min-width: 1200px) and (-ms-high-contrast: none), (-ms-high-contrast: active) { #garfield-' . $aArguments['fileReferenceUid'] . '.tx-kitt3n-image > .aR { background: url(' . (array_key_exists("frontendPathSymlink", $aProcessed[1][0]) ?
                    $aProcessed[1][0]["frontendPathSymlink"] : $aProcessed[1][0]["frontendPath"]) . ') no-repeat; background-size: cover; ' . (str_replace("object-position", "background-position",  $aProcessed[1][0]["style"])) . '}}';

            $sHtml .=
                '<style>' . implode(" ", $aProcessedImageCsss) . '</style>' .
                '<!-- IE 9 and lower -->' .
                '<!--[if lt IE 10]><style type="text/css" media="screen">' . implode(" ", $aProcessedImageFallbackCsss) . '</style><![endif]-->' .
                '<!-- IE 10+ -->' .
                '<style>' . implode(" ", $aProcessedImageIE10PlusFallbackCsss) . '</style>' .
                '<!-- Edge 12-15 -->' .
                '<style>@supports (-ms-ime-align:auto) and (not (display: grid)) {' . implode(" ", $aProcessedImageFallbackCsss) . '}</style>' .
                '<figure class="tx-kitt3n-image" id="garfield-' . $aArguments['fileReferenceUid'] . '">
                    <picture
                        class=" aR ' . implode(' ', $aProcessedImageClasses) . '">
                        <source
                            media="(min-width: 1200px)"
                            data-srcset="' . (array_key_exists("frontendPathSymlink", $aProcessed[1][0]) ?
                    $aProcessed[1][0]["frontendPathSymlink"] : $aProcessed[1][0]["frontendPath"]) . '">
                        <source
                            media="(min-width: 768px)"
                            data-srcset="' . (array_key_exists("frontendPathSymlink", $aProcessed[1][1]) ?
                    $aProcessed[1][1]["frontendPathSymlink"] : $aProcessed[1][1]["frontendPath"]) . '">
                        <img
                            data-src="' . (array_key_exists("frontendPathSymlink", $aProcessed[1][2]) ?
                    $aProcessed[1][2]["frontendPathSymlink"] : $aProcessed[1][02]["frontendPath"]) . '"
                            alt="' . $aProcessed[0]->getAlternative() . '"
                            title="' . $aProcessed[0]->getTitle() . '"
                            class="lazy" >
                        <noscript>
                            <img
                                src="' . (array_key_exists("frontendPathSymlink", $aProcessed[1][2]) ?
                    $aProcessed[1][2]["frontendPathSymlink"] : $aProcessed[1][2]["frontendPath"]) . '"
                                alt="' . $aProcessed[0]->getAlternative() . '"
                                title="' . $aProcessed[0]->getTitle() . '" >
                        </noscript>
                    </picture>';
            // Figcaption
            if($aProcessed[0]->getDescription()){
                $sHtml .=
                    '<figcaption>
                        '. $aProcessed[0]->getDescription() .'
                    </figcaption>';
            }

            $sHtml .= '</figure>';



        } else {
            $sHtml .= "Error";
        }

        return $sHtml;

    }
}