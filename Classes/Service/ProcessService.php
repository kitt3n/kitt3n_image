<?php
namespace KITT3N\Kitt3nImage\Service;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Resource\FileReference as CoreFileReference;
use TYPO3\CMS\Extbase\Domain\Model\FileReference as FileReference;
use TYPO3\CMS\Core\Imaging\ImageManipulation\CropVariantCollection;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class PictureService
 * @package KITT3N\Kitt3nImage\Service
 */
class ProcessService implements \TYPO3\CMS\Core\SingletonInterface
{
    /**
     * @var \TYPO3\CMS\Extbase\Service\ImageService
     * @inject
     */
    protected $imageService = null;

    public function process ($aArguments)
    {
        if (!empty($aArguments['coreFileReference']) && !empty($aArguments['fileReferenceUid'])) {

            /*
             * Should be instanceof CoreFileReference
             * but could be instanceof FileReference
             * (e.g. if it's an image field from ExtensionBuilder)
             *
             * Handle that case properly
             */
            $oFileReferenceOrCoreFileReference = $aArguments['coreFileReference'];
            if ($aArguments['coreFileReference'] instanceof FileReference) {
                $resourceFactory = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Resource\\ResourceFactory');
                /* @var \TYPO3\CMS\Core\Resource\FileReference $oCoreFileReference */
                $oFileReferenceOrCoreFileReference = $resourceFactory->getFileReferenceObject($aArguments['coreFileReference']->getUid());
            }

            if ($oFileReferenceOrCoreFileReference instanceof CoreFileReference) {
                /* @var \TYPO3\CMS\Core\Resource\FileReference $oCoreFileReference */
                $oCoreFileReference = $oFileReferenceOrCoreFileReference;
                $iFileReferenceUid = $aArguments['fileReferenceUid'];

                # Get crop information
                $cropString = $oCoreFileReference->getProperty('crop');
                $cropVariantCollection = CropVariantCollection::create((string)$cropString);

                $aCropAreas = [
                    $cropVariantCollection->getCropArea("default"),
                    $cropVariantCollection->getCropArea("tablet"),
                    $cropVariantCollection->getCropArea("mobile"),
                ];

                $aFocusAreas = [
                    $cropVariantCollection->getFocusArea("default")->asArray(),
                    $cropVariantCollection->getFocusArea("tablet")->asArray(),
                    $cropVariantCollection->getFocusArea("mobile")->asArray(),
                ];

                $aProcessingInstructions = [
                    [
                        'crop' => $aCropAreas[0]->isEmpty() ? null : $aCropAreas[0]->makeAbsoluteBasedOnFile($oCoreFileReference),
                        'maxWidth' => 1920,
                    ],
                    [
                        'crop' => $aCropAreas[1]->isEmpty() ? null : $aCropAreas[1]->makeAbsoluteBasedOnFile($oCoreFileReference),
                        'maxWidth' => 1200,
                    ],
                    [
                        'crop' => $aCropAreas[2]->isEmpty() ? null : $aCropAreas[2]->makeAbsoluteBasedOnFile($oCoreFileReference),
                        'maxWidth' => 768,
                    ]
                ];

                /*
                 * Processed images
                 */
                $aProcessedImages = [];

                /*
                 * Process images
                 */
                foreach ($aProcessingInstructions as $iProcessingInstruction => $aProcessingInstruction) {
                    $aProcessedImages[] = [
                        "processedImage" => $this->imageService->applyProcessingInstructions($oCoreFileReference, $aProcessingInstructions[$iProcessingInstruction]),
                    ];
                }

                /*
                 * Process images
                 */
                foreach ($aProcessingInstructions as $iProcessingInstruction => $aProcessingInstruction) {
                    $aProcessedImages[$iProcessingInstruction]["frontendPath"] =
                        $this->imageService->getImageUri($aProcessedImages[$iProcessingInstruction]["processedImage"]);
                }

                /*
                 * Focus areas
                 */
                foreach ($aProcessingInstructions as $iProcessingInstruction => $aProcessingInstruction) {
                    /*
                     * Focus
                     *
                     *    0   1   2   3   4   5   6   7   8   9   10
                     *  0 x---+---+---+---+---+---+---+---+---+---+
                     *    |   |   |   |   |   |   |   |   |   |###| x: 100% / y: 0%
                     *  1 +---+---+---+---+---+---+---+---+---+---+
                     *    |   |   |   |   |   |   |   |   |   |   |
                     *  2 +---+---+---+---+---+---+---+---+---+---+
                     *    |   |   |   |   |   |   |   |   |   |   |
                     *  3 +---+---+---+---+---+---+---+---+---+---+
                     *    |   |   |   |   |   |   |   |   |   |   |
                     *  4 +---+---+---+---+---+---+---+---+---+---+
                     *    |   |   |   |   |   |   |   |   |   |   |
                     *  5 +---+---+---+---+---+---+---+---+---+---+
                     *    |   |   |   |   |   |   |   |   |   |   |
                     *  6 +---+---+---+---+---+---+---+---+---+---+
                     *    |   |   |   |   |   |   |   |   |   |   |
                     *  7 +---+---+---+---+---+---+---+---+---+---+
                     *    |   |   |   |   |   |   |   |   |   |   |
                     *  8 +---+---+---+---+---+---+---+---+---+---+
                     *    |   |   |   |   |   |   |   |   |   |   |
                     *  9 +---+---+---+---+---+---+---+---+---+---+
                     *    |   |   |   |   |   |   |   |   |   |   |
                     * 10 +---+---+---+---+---+---+---+---+---+---+
                     *
                     */
                    $fX = $aFocusAreas[$iProcessingInstruction]['x'] + ($aFocusAreas[$iProcessingInstruction]['width'] / 2);
                    $fY = $aFocusAreas[$iProcessingInstruction]['y'] + ($aFocusAreas[$iProcessingInstruction]['height'] / 2);
                    $aProcessedImages[$iProcessingInstruction]["style"] =
                         'object-position: ' . $fX * 100 . '% ' . $fY * 100 . '%;';
                    $aProcessedImages[$iProcessingInstruction]["pictureStyle"] = '';
                }

                /*
                 * Get dimensions from processed images
                 */
                foreach ($aProcessingInstructions as $iProcessingInstruction => $aProcessingInstruction) {
                    $aProcessedImages[$iProcessingInstruction]["dimensions"] = [
                        "iWidth" => $aProcessedImages[$iProcessingInstruction]["processedImage"]->getProperty('width'),
                        "iHeight" => $aProcessedImages[$iProcessingInstruction]["processedImage"]->getProperty('height'),
                    ];
                }

                /*
                 * Get classes and pictureStyle information for (free) processed images
                 */
                foreach ($aProcessedImages as $iProcessedImage => $aProcessedImage) {
                    $aAspectRatios = $this->getAspectRatios(
                        $iProcessedImage,
                        $aProcessedImages[$iProcessedImage]["dimensions"]["iWidth"],
                        $aProcessedImages[$iProcessedImage]["dimensions"]["iHeight"]
                    );
                    $aProcessedImages[$iProcessedImage]["classes"] = $aAspectRatios[0];

                    if (array_key_exists(2, $aAspectRatios)) {
                        // free aspect ratio
                        $aProcessedImages[$iProcessedImage]["pictureStyle"] .=
                            'padding-bottom: ' . $aAspectRatios[2] . '%;';
                    }

                }

//                $sapi_type = strtolower(php_sapi_name());
//                if (strpos($sapi_type, "cgi") !== false) {
                    $dr = rtrim($_SERVER['DOCUMENT_ROOT'], '/');
                    $fs = new Filesystem();
                    foreach ($aProcessedImages as $iProcessedImage => $aProcessedImage) {

                        /*
                         * Symlink directory
                         */
                        $aSymlinkDirectory = [
                            $dr,
                            'pimages',
                            $iProcessedImage,
                            $iFileReferenceUid,
                        ];

                        /*
                         * Path to processed image
                         */
                        $aProcessedImagePath = [
                            $dr,
                            ltrim($aProcessedImage["frontendPath"], '/'),
                        ];
                        $sProcessedImagePath = implode("/", $aProcessedImagePath);

                        /*
                         * Symlink path to processed image
                         */
                        $aImageSymlinkPath = [
                            $dr,
                            'pimages',
                            $iProcessedImage,
                            $iFileReferenceUid,
                            $oCoreFileReference->getName(),
                        ];
                        $sImageSymlinkPath = implode("/", $aImageSymlinkPath);

                        /*
                         * Create symlink if it does not exist or image has been changed
                         *
                         * - if $path does not exist or is not a link, it returns null.
                         * - if $path is a link, it returns the next direct target of the link without
                         *   considering the existence of the target.
                         */
                        $sSymlinkTarget = $fs->readlink($sImageSymlinkPath, false);
                        if ($sSymlinkTarget === null) {

                            /*
                             * Create symlink directory
                             *
                             * This function ignores already existing directories.
                             */
                            $fs->mkdir(implode("/", $aSymlinkDirectory));

                            /*
                             * Create symlink
                             */
                            if (symlink($sProcessedImagePath, $sImageSymlinkPath)) {

                                /*
                                 * Remove DOCUMENT_ROOT from symlink path for frontend
                                 */
                                $aProcessedImages[$iProcessedImage]["frontendPathSymlink"] = str_replace($dr, "", $sImageSymlinkPath);
                            }

                        } elseif ($sProcessedImagePath != $sSymlinkTarget) {

                            /*
                             * Delete old symlink
                             */
                            $fs->remove([
                                $sImageSymlinkPath
                            ]);

                            /*
                             * Create symlink directory
                             *
                             * This function ignores already existing directories.
                             */
                            $fs->mkdir($dr . '/' . 'pimages' . '/' . $iProcessedImage . '/' . $iFileReferenceUid);

                            /*
                             * Create new symlink
                             */
                            if (symlink($sProcessedImagePath, $sImageSymlinkPath)) {

                                /*
                                 * Remove DOCUMENT_ROOT from symlink path for frontend
                                 */
                                $aProcessedImages[$iProcessedImage]["frontendPathSymlink"] = str_replace($dr, "", $sImageSymlinkPath);
                            }

                        } else {

                            $aProcessedImages[$iProcessedImage]["frontendPathSymlink"] = str_replace($dr, "", $sImageSymlinkPath);

                        }
                    }
//                }

                return [
                    $oCoreFileReference,
                    $aProcessedImages,
                ];

            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    private function getAspectRatios($i, $iWidth, $iHeight)
    {
        $iPrecision = 1;
        $iAspectRatio = round($iWidth / $iHeight, $iPrecision);
        switch ($iAspectRatio) {
            case round(21 / 5, $iPrecision):
                return
                    [
                        "aR--$i-21_5",
                        "21_5",
                    ];
                break;
            case round(21 / 9, $iPrecision):
                return
                    [
                        "aR--$i-21_9",
                        "21_9",
                    ];
                break;
            case round(16 / 9, $iPrecision):
                return
                    [
                        "aR--$i-16_9",
                        "16_9",
                    ];
                break;
            case round(4 / 3, $iPrecision):
                return
                    [
                        "aR--$i-4_3",
                        "4_3",
                    ];
                break;
            case round(3 / 2, $iPrecision):
                return
                    [
                        "aR--$i-3_2",
                        "3_2",
                    ];
                break;
            case round(1 / 1, $iPrecision):
                return
                    [
                        "aR--$i-1_1",
                        "1_1",
                    ];
                break;
            case round(2 / 3, $iPrecision):
                return
                    [
                        "aR--$i-2_3",
                        "2_3",
                    ];
                break;
            case round(3 / 4, $iPrecision):
                return
                    [
                        "aR--$i-3_4",
                        "3_4",
                    ];
                break;
            default:
                $fPaddingBottom = 100 * $iHeight / $iWidth;
                return
                    [
                        "aR--$i-x_y",
                        "$iWidth" . "_" . "$iHeight",
                        $fPaddingBottom,
                    ];
        }
    }
}