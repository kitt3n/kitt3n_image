<?php
namespace KITT3N\Kitt3nImage\Service;

/**
 * Class PictureService
 * @package KITT3N\Kitt3nImage\Service
 */
class LazyBackgroundImageService implements \TYPO3\CMS\Core\SingletonInterface
{
    /**
     * processService
     *
     * @var \KITT3N\Kitt3nImage\Service\ProcessService
     * @inject
     */
    protected $processService = null;

    public function returnLazyBackgroundImageHtml ($aArguments)
    {

        $sHtml = '';

        $aProcessed = $this->processService->process($aArguments);

        if ( ! null == $aProcessed) {

            $aProcessedImageClasses = [];
            foreach ($aProcessed[1] as $iProcessedImage => $aProcessedImage) {
                $aProcessedImageClasses[] = $aProcessedImage["classes"];
            }
            
            $aProcessedImageCss = [];

            $aProcessedImageCss[] =
                '#garfield-' . $aArguments['fileReferenceUid'] . '.tx-kitt3n-image > .aR { display: none; }';

            /*
             * small
             */
            $aProcessedImageCss[] =
                '@media (max-width: 991px) { #garfield-' . $aArguments['fileReferenceUid'] . '.tx-kitt3n-image > .aR.aR--1-2 { display: block; background-size: cover; ' .
                (str_replace("object-position", "background-position",  $aProcessed[1][2]["style"])) . '}}';

            /*
             * large
             */
            $aProcessedImageCss[] =
                '@media (min-width: 992px) and (max-width: 1199px) { #garfield-' . $aArguments['fileReferenceUid'] . '.tx-kitt3n-image > .aR.aR--1-1 { display: block; background-size: cover; ' . (str_replace("object-position", "background-position",  $aProcessed[1][1]["style"])) . '}}';

            /*
             * extra large
             */
            $aProcessedImageCss[] =
                '@media (min-width: 1200px) { #garfield-' . $aArguments['fileReferenceUid'] . '.tx-kitt3n-image > .aR.aR--1-0 { display: block; background-size: cover; ' . (str_replace("object-position", "background-position",  $aProcessed[1][0]["style"])) . '}}';

            $sHtml .=
                '<style>' . implode(" ", $aProcessedImageCss) . '</style>' .
                '<div class="tx-kitt3n-image tx-kitt3n-background-image" id="garfield-' . $aArguments['fileReferenceUid'] . '">' .
                    '<div class="aR aR--1-0 lazy" data-src="' . (array_key_exists("frontendPathSymlink", $aProcessed[1][0]) ?
                        $aProcessed[1][0]["frontendPathSymlink"] : $aProcessed[1][0]["frontendPath"]) . '"></div>' .
                    '<div class="aR aR--1-1 lazy" data-src="' . (array_key_exists("frontendPathSymlink", $aProcessed[1][1]) ?
                        $aProcessed[1][1]["frontendPathSymlink"] : $aProcessed[1][1]["frontendPath"]) . '"></div>' .
                    '<div class="aR aR--1-2 lazy" data-src="' . (array_key_exists("frontendPathSymlink", $aProcessed[1][2]) ?
                        $aProcessed[1][2]["frontendPathSymlink"] : $aProcessed[1][2]["frontendPath"]) . '"></div>';
            $sHtml .= '</div>';



        } else {
            $sHtml .= "Error";
        }

        return $sHtml;

    }
}